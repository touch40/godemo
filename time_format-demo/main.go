package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println(time.Now().Format("2006年01月"))
	fmt.Println(time.Now().Format("2006-01-02 15:04:05"))
	fmt.Println(time.Now().Format("2006-01-02 15:04:05"))

	t := time.Now()
	t2 := t.AddDate(1, 1, 1)
	fmt.Println(t2)
}
