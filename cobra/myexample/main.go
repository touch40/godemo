/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package main

import "gitee.com/touch/godemo/cobra/myexample/cmd"

func main() {
	cmd.Execute()
}

//  go build [-o output] [build flags] [packages]
