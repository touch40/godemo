package main

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"gitee.com/touch40/godemo/rabbitmq/rabbitmq"

	"github.com/streadway/amqp"
)

func product() {
	rabbitmq.InitRabbitMq()
	mq := rabbitmq.RabbitMqMap["mq"] // 可以定义基本的exchange类型，topic(模糊匹配),direct
	err := mq.PrepareExchange("topic_exchange", "topic")

	if err != nil {
		log.Fatal("准备交换机出错", err)
	}

	queue, err := mq.PrepareQueue("test-qq")
	if err != nil {
		fmt.Println("queue初始化失败", err.Error())
		log.Fatal(queue.Name)
	}
	queue2, err := mq.PrepareQueue("test-qq2")
	if err != nil {
		fmt.Println("queue2初始化失败", err.Error())
		log.Fatal(queue2.Name)
	}
	queue3, err := mq.PrepareQueue("test-qq3")
	if err != nil {
		fmt.Println("queue3初始化失败", err.Error())
		log.Fatal(queue3.Name)
	}
	if err := mq.QueueBindExchange("test-qq", "wodekey.log.info", "topic_exchange"); err != nil {
		log.Fatal("队列绑定交换机出错", err)
	}
	if err := mq.QueueBindExchange("test-qq2", "wodekey.log.debug", "topic_exchange"); err != nil {
		log.Fatal("队列绑定交换机出错2", err)
	}
	if err := mq.QueueBindExchange("test-qq3", "wodekey.log.error", "topic_exchange"); err != nil {
		log.Fatal("队列绑定交换机出错2", err)
	}

	for i := 0; i < 1000; i++ {
		//mq.QueueSend("test-qq", amqp.Publishing{
		//    AppId:       "",
		//    ContentType: "application/json",
		//    MessageId: "你好",
		//    Body:        []byte("这是我的消息:" + strconv.Itoa(i)),
		//})
		//fmt.Println("发送成功: test-qq  ", i)
		//time.Sleep(2 * time.Second)
		//mq.QueueSend("test-qq2", amqp.Publishing{
		//    AppId:       "",
		//    ContentType: "application/json",
		//    MessageId: "你好啊",
		//    Body:        []byte("这是我的消息2:" + strconv.Itoa(i)),
		//})

		mq.ExchangeSend("topic_exchange", "wodekey.random", amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte("这是我的消息哦" + strconv.Itoa(i)),
		})
		fmt.Println("发送成功: exchange  ", i)
		time.Sleep(1 * time.Second)
	}

}

func main() {
	product()

	time.Sleep(1000000 * time.Second)
}
