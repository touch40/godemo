package main

import (
	"fmt"
	"log"
	"time"

	"gitee.com/touch40/godemo/rabbitmq/rabbitmq"
)

func consumer() {
	rabbitmq.InitRabbitMq()
	mq := rabbitmq.RabbitMqMap["mq2"]
	if err := mq.QueueBindExchange("test-qq", "*.log.info", "topic_exchange"); err != nil {
		log.Fatal("队列绑定交换机出错", err)
	}
	if err := mq.QueueBindExchange("test-qq2", "*.log.debug", "topic_exchange"); err != nil {
		log.Fatal("队列绑定交换机出错2", err)
	}
	if err := mq.QueueBindExchange("test-qq3", "*.log.error", "topic_exchange"); err != nil {
		log.Fatal("队列绑定交换机出错2", err)
	}
	go func() {
		resu, err := mq.QueueConsume("test-qq", "consumer1")
		if err != nil {
			fmt.Println("消费错误", err.Error())
			log.Fatal("test-qq消费错误")
		}
		for d := range resu {
			fmt.Println(d.ConsumerTag+" test-qq消费成功:", string(d.Body))

			d.Ack(true) //需手动应答
			time.Sleep(2 * time.Second)
		}
	}()

	go func() {
		resu, err := mq.QueueConsume("test-qq2", "consumer002")
		if err != nil {
			fmt.Println("消费错误", err.Error())
			log.Fatal("test-qq2消费错误")
		}
		for d := range resu {
			fmt.Println(d.ConsumerTag+" test-qq2消费成功:", string(d.Body))
			d.Ack(true) //需手动应答
			time.Sleep(2 * time.Second)
		}
	}()

	go func() {
		resu, err := mq.QueueConsume("test-qq3", "consumer003")
		if err != nil {
			fmt.Println("消费错误", err.Error())
			log.Fatal("test-qq3消费错误")
		}
		for d := range resu {
			fmt.Println(d.ConsumerTag+" test-qq3消费成功:", string(d.Body))
			d.Ack(true) //需手动应答
			time.Sleep(2 * time.Second)
		}
	}()
}

func main() {
	consumer()

	time.Sleep(10000000 * time.Second)
}
