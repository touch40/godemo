package main

import (
	"fmt"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

type database struct {
	Name string
	Pwd  string
}

func main() {
	v := viper.New()
	v.SetConfigName("config") // 配置文件名 (不带扩展格式)
	v.SetConfigType("yml")    // 如果你的配置文件没有写扩展名，那么这里需要声明你的配置文件属于什么格式
	v.AddConfigPath("./")     // 配置文件的路径
	err := v.ReadInConfig()   //找到并读取配置文件
	if err != nil {           // 捕获读取中遇到的error
		panic(fmt.Errorf("fatal error config file: %w ", err))
	}
	v.Set("test1", "test1")
	// v.Set("database", database{
	// 	"hms", "hmsr",
	// })
	fmt.Println(v.Get("test1"))
	var conf database
	// m := v.Get("database")
	// conf := m.(database)
	v.Unmarshal(&conf)
	fmt.Println(conf.Pwd)
	// viper.WriteConfigAs("/path/to/my/.config")
	v.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	v.WatchConfig()
	v.WriteConfig()

}
