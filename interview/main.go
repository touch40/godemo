package main

import (
	"fmt"
)

// 3个函数分别打印cat、dog、fish，要求每个函数都要起一个goroutine，按照cat、dog、fish顺序打印在屏幕上100次。

func main() {
	c1 := make(chan bool)
	c2 := make(chan bool)
	c3 := make(chan bool)
	c4 := make(chan bool)
	go cat(c1, c2)
	go dog(c2, c3)
	go fish(c3, c4)
	for i := 0; i < 100; i++ {
		c1 <- true
		<-c4
		// fmt.Println(i+1)
	}
}

func cat(cin <-chan bool, cout chan<- bool) {
	for range cin {
		fmt.Println("cat")
		cout <- true
	}
}

func dog(c <-chan bool, cout chan<- bool) {
	for range c {
		fmt.Println("dog")
		cout <- true
	}
}

func fish(c <-chan bool, cout chan<- bool) {
	for range c {
		fmt.Println("fish")
		cout <- true
	}
}
