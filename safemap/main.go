package main

import (
	"sync"
)

type safeMap struct {
	mu *sync.Mutex
	m  map[int]int
}

func NewSafeMap() *safeMap {
	return &safeMap{
		mu: &sync.Mutex{},
		m:  map[int]int{},
	}
}

func main() {
	sm := &safeMap{}
	go func() {
		sm.mu.Lock()
		sm.m[10] = 1
		sm.mu.Unlock()
	}()
	go func() {
		sm.mu.Lock()
		sm.m[10] = 1
		sm.mu.Unlock()
	}()
	
}
