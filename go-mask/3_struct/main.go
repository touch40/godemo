package main

import (
	"fmt"

	mask "github.com/showa-93/go-mask"
)

func main() {
	value := struct {
		Title string   `mask:"filled"`
		Casts []string `mask:"fixed"`
	}{
		Title: "Catch Me If You Can",
		Casts: []string{
			"Thomas Jeffrey \"Tom\" Hanks",
			"Leonardo Wilhelm DiCaprio",
		},
	}

	maskValue, _ := mask.Mask(value)
	fmt.Printf("value:%+v\n", value)
	fmt.Printf("maskValue:%+v\n", maskValue) //脱敏字符输出符号是：*

	masker := mask.NewMasker()
	masker.SetMaskChar("-") //自定义脱敏字符为 -

	// 给对应的tag注册对应的脱敏处理函数
	masker.RegisterMaskStringFunc(mask.MaskTypeFilled, masker.MaskFilledString)
	masker.RegisterMaskStringFunc(mask.MaskTypeFixed, masker.MaskFixedString)

	maskValue2, _ := masker.Mask(value)

	fmt.Printf("maskValue2:%+v\n", maskValue2)
}
