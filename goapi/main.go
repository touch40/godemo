package main

import (
	"fmt"
	"io"
	"net/http"
)

func main() {
	client := &http.Client{}
	request, _ := http.NewRequest("GET", "http://localhost:8088/ping2", nil)
	// request.URL.
	response, err := client.Do(request)
	if err != nil {
		fmt.Println(err)
	}
	defer response.Body.Close()
	if response.StatusCode == 200 {
		body, _ := io.ReadAll(response.Body)
		bodystr := string(body)
		fmt.Printf("%v\n", bodystr)
	}
	
}
