package main

import (
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofrs/uuid"
)

type Claims struct {
	UserId             uuid.UUID `json:"user_id"` //用户id
	jwt.StandardClaims           //标准的jwt的claims结构
}
type User struct {
	ID uuid.UUID `gorm:"type:uuid;primary_key;default:uuid_generate_v4()" json:"id"`
}

var jwtKey = []byte("kai_yuan_shi_nian")

func main() {
	a, err := ReleaseToken(User{ID: uuid.Must(uuid.NewV4())})
	if err != nil {
		panic(err)
	}
	fmt.Println(a)
	time.After(time.Second * 10)
}

func ReleaseToken(user User) (string, error) {
	expirationTime := time.Now().Add(7 * 24 * time.Hour)
	claims := &Claims{
		UserId: user.ID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(), //设置这个token的有效期
			IssuedAt:  time.Now().Unix(),     //发放时间
			Issuer:    "kaiyuanshinian.tech", //发行方
			Subject:   "user token",          //主题
		},
	}
	//使用指定的签名方式创建签名对象
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	//使用上面指定的钥匙(secret)签名并获取完整的签名后的字符串
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}
