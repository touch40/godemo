package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.Use(gin.Recovery())
	r.GET("/demo1", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"demo": "this is hello demo"})
	})

	// 此规则能够匹配/demo2/john这种格式，但不能匹配/demo2/ 或 /user这种格式
	r.GET("/demo2/:name", func(c *gin.Context) {
		name := c.Param("name")
		c.String(http.StatusOK, "Hello %s", name)
	})

	// 但是，这个规则既能匹配/demo2/john/格式也能匹配/demo2/john/send这种格式
	// 如果没有其他路由器匹配/demo2/john，它将重定向到/demo2/john/
	// action会匹配到「/」
	r.GET("/demo2/:name/*action", func(c *gin.Context) {
		name := c.Param("name")
		action := c.Param("action")
		message := name + " is " + action
		c.String(http.StatusOK, message)
	})

	// 匹配的url格式:  /welcome?firstname=Jane&lastname=Doe
	r.GET("/demo3", func(c *gin.Context) {
		firstname := c.DefaultQuery("firstname", "Guest")
		lastname := c.Query("lastname") // 是 c.Request.URL.Query().Get("lastname") 的简写

		c.String(http.StatusOK, "Hello %s %s", firstname, lastname)
	})

	// POST参数获得form
	r.POST("/demo4", func(c *gin.Context) {
		message := c.PostForm("message")
		nick := c.DefaultPostForm("nick", "anonymous") // 此方法可以设置默认值
		c.JSON(200, gin.H{
			"status":  "posted",
			"message": message,
			"nick":    nick,
		})
	})

	// POST参数获得json
	type testMsg struct {
		Nick    string `json:"nick"`
		Message string `json:"message"`
	}
	r.POST("/demo5", func(c *gin.Context) {
		var u testMsg
		err := c.ShouldBindJSON(&u)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		}
		db := os.Getenv("DEBUG")
		if db != "" {
			println("Debug mode on")
		} else {
			println("No Debug")
		}
		message := u.Nick
		nick := u.Message // 此方法可以设置默认值
		c.JSON(200, gin.H{
			"status":  "posted",
			"message": message,
			"nick":    nick,
		})
	})
	// uri
	type Person struct {
		ID   string `uri:"id" binding:"required,uuid"`
		Name string `uri:"name" binding:"required"`
	}
	r.GET("/:name/:id", func(c *gin.Context) {
		var person Person
		if err := c.ShouldBindUri(&person); err != nil {
			c.JSON(400, gin.H{"msg": err})
			return
		}
		c.JSON(200, gin.H{"name": person.Name, "uuid": person.ID})
	})

	// ProtoBuf
	// r.GET("/someProtoBuf", func(c *gin.Context) {
	// 	reps := []int64{int64(1), int64(2)}
	// 	label := "test"
	// 	// The specific definition of protobuf is written in the testdata/protoexample file.
	// 	data := &protoexample.Test{
	// 		Label: &label,
	// 		Reps:  reps,
	// 	}

	// Note that data becomes binary data in the response
	// Will output protoexample.Test protobuf serialized data
	// 	c.ProtoBuf(http.StatusOK, data)
	// })

	r.Static("/assets", "./assets")
	// router.StaticFS("/more_static", http.Dir("my_file_system"))
	// router.StaticFile("/favicon.ico", "./resources/favicon.ico")
	// 重点
	r.GET("/someDataFromReader", func(c *gin.Context) {
		response, err := http.Get("https://raw.githubusercontent.com/gin-gonic/logo/master/color.png")
		if err != nil || response.StatusCode != http.StatusOK {
			c.Status(http.StatusServiceUnavailable)
			return
		}

		reader := response.Body
		contentLength := response.ContentLength
		contentType := response.Header.Get("Content-Type")

		extraHeaders := map[string]string{
			"Content-Disposition": `attachment; filename="gopher.png"`,
		}

		c.DataFromReader(http.StatusOK, contentLength, contentType, reader, extraHeaders)
	})

	r.GET("/test", func(c *gin.Context) {
		c.Request.URL.Path = "/test2"
		r.HandleContext(c)
	})
	r.GET("/test2", func(c *gin.Context) {
		c.JSON(200, gin.H{"hello": "world"})
	})

	r.GET("/test3", func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "http://www.google.com/")
	})
	// 重点，在中间件或处理程序中启动新的Goroutines时,你不应该使用其中的原始上下文,你必须

	r.GET("/long_async", func(c *gin.Context) {
		// 创建要在goroutine中使用的副本
		cCp := c.Copy()
		go func() {
			// simulate a long task with time.Sleep(). 5 seconds
			time.Sleep(5 * time.Second)

			// 这里使用你创建的副本
			log.Println("Done! in path " + cCp.Request.URL.Path)
		}()
	})

	// 重点 自定义HTML
	// http.ListenAndServe(":8080", router)
	// router := gin.Default()

	// s := &http.Server{
	// 	Addr:           ":8080",
	// 	Handler:        router,
	// 	ReadTimeout:    10 * time.Second,
	// 	WriteTimeout:   10 * time.Second,
	// 	MaxHeaderBytes: 1 << 20,
	// }
	// s.ListenAndServe()
	r.Run(":8888")
}
