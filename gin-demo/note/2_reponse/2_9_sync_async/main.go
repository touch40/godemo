package main

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/sync", func(c *gin.Context) {
		sync(c)
		c.JSON(http.StatusOK, ">>>主程序（主go程序）异步已经执行<<<")
	})
	r.GET("/async", func(c *gin.Context) {
		for i := 0; i < 6; i++ {
			cCp := c.Copy()
			go async(cCp, i)
		}
		c.JSON(http.StatusOK, ">>>主程序（主go程序）同步已经执行<<<")
	})
	r.Run(":9090")
}

func async(cp *gin.Context, i int) {
	fmt.Println("第" + strconv.Itoa(i) + "开始执行任务：" + cp.Request.URL.Path)
	time.Sleep(time.Second + 2)
	fmt.Println("第" + strconv.Itoa(i) + "执行完成")
}

func sync(c *gin.Context) {
	fmt.Println("开始执行同步任务：" + c.Request.URL.Path)
	time.Sleep(time.Second)
	fmt.Println("同步执行完成")
}
