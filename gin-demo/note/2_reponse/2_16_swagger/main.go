package main

import (
	"fmt"
	"net/http"

	_ "gitee.com/touch40/godemo/gindemo/note/2_reponse/2_16_swagger/docs"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

type User struct {
	UserName string `json:"user_name"`
	Password string `json:"password"`
}
type Response struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data string `json:"data"`
}

func main() {
	r := gin.Default()
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.GET("/login", login)
	r.POST("/register", register)
	r.Run(":9090")
}

// @tags 注册接口
// @Summary 注册
// @Description register
// @Accept json
// @Produce json
// @Param username formData string true "用户名"
// @Param password formData string false "密码"
// @Success 200 {string} json "{"code":200,"data":"{"name":"username","password":"password"}","msg":"OK"}"
// @Router /register [post]
func register(c *gin.Context) {
	var user User
	err := c.Bind(&user)
	if err != nil {
		fmt.Println("绑定错误：", err)
		c.JSON(http.StatusBadRequest, "数据错误！")
	}
	res := Response{
		Code: http.StatusOK,
		Msg:  "注册成功",
		Data: "OK",
	}
	c.JSON(http.StatusOK, res)
}

// @tags 登录接口
// @Summary 登录
// @Description login
// @Accept json
// @Produce json
// @Param username query string true "用户名"
// @Param password query string false "密码"
// @Success 200 {string} json "{"code":200,"data":"{"name":"username","password":"password"}","msg":"OK"}"
// @Router /login [get]
func login(c *gin.Context) {
	userName := c.Query("name")
	pwd := c.Query("pwd")
	fmt.Println(userName, pwd)
	res := Response{}
	res.Code = http.StatusOK
	res.Data = "OK"
	c.JSON(http.StatusOK, res)
}

// 1、添加依赖
// go get github.com/swaggo/swag/cmd/swag
// go get github.com/swaggo/gin-swagger
// go get github.com/swaggo/files

// 2、给Handler对应方法添加注释
// @Tags:说明该方法的作用
// @Summary 登录
// @Description:这个API详细描述
// @Accept：表示该请求的请求类型
// @Produce：返回数据类型
// @Param：参数，表示需要传递到服务器端的参数
// @Success：成功返回给客户端的信息
// @Router：路由信息

// 注释符 @符号
