package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type UserAPI struct {
	UserName string `json:"user_name" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type TempData struct {
	Msg  string `json:"msg"`
	Data string `json:"data"`
}

func main() {
	url := "http://127.0.0.1:9090/login"
	user := UserAPI{"user", "123456"}
	data, err := getRestFulAPI(url, user, "application/json")
	var temp TempData
	json.Unmarshal(data, &temp)
	fmt.Println(data, err)
	fmt.Println(temp)
}

// 发送POST请求
// url:请求地址
// data:POST请求提交的数据
// contentType:请求体的格式，如：application/json
// content:请求放回的内容
func getRestFulAPI(url string, data any, contentType string) ([]byte, error) {
	// 创建调用API接口的client
	client := &http.Client{Timeout: 5 * time.Second}
	jsonStr, _ := json.Marshal(data)
	resp, err := client.Post(url, contentType, bytes.NewBuffer(jsonStr))
	if err != nil {
		fmt.Println("调用API接口出现了错误！")
		return nil, err
	}
	res, err := ioutil.ReadAll(resp.Body)
	return res, err
}
