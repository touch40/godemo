// 请求第三方数据
// response,err := http.Get(url)

// 获取响应体
// body := response.Body

// 数据返回Client
// c.DataFromReader(http.StatusOK,contentLength,contentType,body,extraHeaders)
package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/GetOtherData", func(c *gin.Context) {
		url := "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Flmg.jj20.com%2Fup%2Fallimg%2Fsj01%2F20121R10U14154-0-lp.jpg&refer=http%3A%2F%2Flmg.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1668852000&t=580596c2592f116a7460a95c1d9dd401"
		response, err := http.Get(url)
		if err != nil || response.StatusCode != http.StatusOK {
			c.Status(http.StatusServiceUnavailable) // 应答client
			return
		}

		body := response.Body
		contentLength := response.ContentLength
		contentType := response.Header.Get("Content-Type")
		c.DataFromReader(http.StatusOK, contentLength, contentType, body, nil)
	})
	r.Run(":9090")
}
