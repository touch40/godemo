package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.POST("/upload", func(c *gin.Context) {
		form, err := c.MultipartForm() // 获取form
		if err != nil {
			c.String(http.StatusBadRequest, "上传文件错误！")
		}
		files := form.File["file_key"] // 上传所有文件
		dst := "E:/"
		for _, file := range files {
			fmt.Println("文件：" + file.Filename + "\t上传完成")
			// 上传文件到指定目录
			c.SaveUploadedFile(file, dst+file.Filename)
		}
		c.String(http.StatusOK, fmt.Sprintf("%d files uploaded!", len(files)))
	})
	r.Run(":9090")
}
