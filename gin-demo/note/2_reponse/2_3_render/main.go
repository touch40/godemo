package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default() // 路由引擎
	// json输出
	r.GET("/json", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"html": "<b>hello,Gin框架</b>",
		})
	})
	// 原样输出
	r.GET("/someHTML", func(c *gin.Context) {
		c.PureJSON(http.StatusOK, gin.H{
			"html": "<b>hello,Gin框架</b>",
		})
	})

	r.GET("/someXML", func(c *gin.Context) {
		type Message struct {
			Name string
			Msg  string
			Age  int
		}
		info := Message{}
		info.Name = "阿晓"
		info.Msg = "hello"
		info.Age = 23
		c.XML(http.StatusOK, info)
	})

	r.GET("/someYAML", func(c *gin.Context) {
		c.YAML(http.StatusOK, gin.H{
			"message": "Gin框架的多形式渲染",
			"status":  200,
		})
	})

	r.GET("/someYAML2", func(c *gin.Context) {
		type Message struct {
			Name string
			Msg  string
			Age  int
		}
		info := Message{}
		info.Name = "阿晓"
		info.Msg = "hello"
		info.Age = 23
		c.XML(http.StatusOK, info)
	})

	r.Run(":9090")
}

// 返回json
// c.JSON(http.StatusOK,gin.H{"html":"<b>Hello,江州！</b>"})
// 原样输出html
// c.PureJSON(http.StatusOK,gin.H{"html":"<b>Hello,江州！</b>"})
//返回YAML形式
// c.YAML(http.StatusOK,gin.H{"html":"<b>Hello,江州！</b>"})
//返回XML
// c.XML(http.StatusOK,gin.H{"html":"<b>Hello,江州！</b>"})
