package main

import (
	"fmt"
	"net/http"
	"unicode/utf8"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	uuid "github.com/satori/go.uuid"
)

type Address struct {
	Province string `validate:"required"`
	City     string `validate:"required"`
	Phone    string `validate:"numeric,len=11"`
}

type UserInfo struct {
	Id      string `json:"id" validate:"uuid"`
	Name    string `json:"name" validate:"checkName"`
	Address string `json:"address" validate:"min=0,max=130"`
}

var validate *validator.Validate

func init() {
	validate = validator.New()
	validate.RegisterValidation("checkName", checkNameFunc)
}
func checkNameFunc(f validator.FieldLevel) bool {
	count := utf8.RuneCountInString(f.Field().String())
	if count >= 2 && count <= 12 {
		return true
	}
	return false

}
func main() {
	uuid := uuid.Must(uuid.NewV4(), nil)
	fmt.Println(uuid.String())
	r := gin.Default()
	var user UserInfo
	r.POST("/validate", func(c *gin.Context) {
		err := c.Bind(&user)
		if err != nil {
			c.JSON(http.StatusBadRequest, "请求参数错误！")
			return
		}
		// 校验
		err = validate.Struct(user)
		if err != nil {
			for _, e := range err.(validator.ValidationErrors) {
				fmt.Println("错误的字段：", e.Field())
				fmt.Println("错误的值：", e.Value())
				fmt.Println("错误的tag：", e.Tag())
			}
			c.JSON(http.StatusBadRequest, "数据校验失败")
			return
		}
		c.JSON(http.StatusOK, "数据成功！")
	})
	r.Run(":9090")
}

// go-playground/validator
