package main

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Product struct {
	ID             int    `gorm:"primaryKey;utoIncrement" json:"id"`
	Number         string `gorm:"unique" json:"number"`
	Category       string `gorm:"type:varchar(256);not null" json:"category"`
	Name           string `gorm:"type:varchar(20);not null" json:"name"`
	MadeIn         string `gorm:"type:varchar(128);not null)" json:"made_in"`
	ProductionTime string `gorm:"column:production_time" json:"production_time"`
}

// 应答体
type GormResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Data    any    `json:"data"`
}

var gormDB *gorm.DB
var gormResponse GormResponse

func init() {
	var err error
	sqlStr := "root:touch@40@tcp(127.0.0.1:3306)/testdb?charset=utf8&parseTime=true&loc=Local"
	gormDB, err = gorm.Open(mysql.Open(sqlStr), &gorm.Config{})
	if err != nil {
		log.Fatal("数据库连接出现问题", err)
	}
	err = gormDB.AutoMigrate(&Product{})
	if err != nil {
		log.Fatal("数据库建表", err)
	}
}

func main() {
	r := gin.Default()
	g := r.Group("gorm")
	{
		g.POST("insert", gormInsertData)
		g.GET("get", gormGetData)
		g.GET("getmul", gormGetMulData)
		g.POST("update", gormUpdateData)
		g.DELETE("delete", gormDeleteData)
	}
	r.Run(":9090")
}

func gormGetMulData(c *gin.Context) {
	m := c.Query("made_in")
	products := make([]Product, 10)
	tx := gormDB.Where("made_in=?", m).Find(&products).Limit(10)
	if tx.Error != nil {
		gormResponse.Code = http.StatusBadRequest
		gormResponse.Message = "查询错误"
		gormResponse.Data = tx.Error
		c.JSON(http.StatusOK, gormResponse)
		return
	}
	gormResponse.Code = http.StatusOK
	gormResponse.Message = "查询成功"
	gormResponse.Data = products
	c.JSON(http.StatusOK, gormResponse)
}

func gormGetData(c *gin.Context) {
	number := c.Query("number")
	product := Product{}
	tx := gormDB.Where("number=?", number).First(&product)
	if tx.Error != nil {
		gormResponse.Code = http.StatusBadRequest
		gormResponse.Message = "查询错误"
		gormResponse.Data = tx.Error
		c.JSON(http.StatusOK, gormResponse)
		return
	}
	gormResponse.Code = http.StatusOK
	gormResponse.Message = "查询成功"
	gormResponse.Data = product
	c.JSON(http.StatusOK, gormResponse)
}
func gormUpdateData(c *gin.Context) {
	// 捕获错误值
	defer func() {
		if err := recover(); err != nil {
			gormResponse.Code = http.StatusBadRequest
			gormResponse.Message = "错误"
			gormResponse.Data = err
			c.JSON(http.StatusOK, gormResponse)
		}
	}()
	var p Product
	err := c.Bind(&p)
	if err != nil {
		gormResponse.Code = http.StatusBadRequest
		gormResponse.Message = "参数错误"
		gormResponse.Data = err
		c.JSON(http.StatusOK, gormResponse)
		return
	}
	//1.先查询
	var count int64
	gormDB.Model(&Product{}).Where("number=?", p.Number).Count(&count)
	if count <= 0 {
		gormResponse.Code = http.StatusBadRequest
		gormResponse.Message = "数据不存在"
		gormResponse.Data = "error"
		c.JSON(http.StatusOK, gormResponse)
	}
	// 2.在更新
	tx := gormDB.Model(&Product{}).Where("number=?", p.Number).Updates(&p)
	if tx.RowsAffected > 0 {
		gormResponse.Code = http.StatusBadRequest
		gormResponse.Message = "更新成功"
		gormResponse.Data = "OK"
		c.JSON(http.StatusOK, gormResponse)
		return
	}
	gormResponse.Code = http.StatusBadRequest
	gormResponse.Message = "数据不存在"
	gormResponse.Data = "error"
	c.JSON(http.StatusOK, gormResponse)
}

func gormDeleteData(c *gin.Context) {
	number := c.Query("number")
	//1.先查询
	var count int64
	gormDB.Model(&Product{}).Where("number=?", number).Count(&count)
	if count <= 0 {
		gormResponse.Code = http.StatusBadRequest
		gormResponse.Message = "数据不存在"
		gormResponse.Data = "error"
		c.JSON(http.StatusOK, gormResponse)
	}
	// 2.在更新
	tx := gormDB.Model(&Product{}).Where("number=?", number).Delete(&Product{})
	if tx.RowsAffected > 0 {
		gormResponse.Code = http.StatusBadRequest
		gormResponse.Message = "删除成功"
		gormResponse.Data = "OK"
		c.JSON(http.StatusOK, gormResponse)
		return
	}
	gormResponse.Code = http.StatusBadRequest
	gormResponse.Message = "删除失败"
	gormResponse.Data = "error"
	c.JSON(http.StatusOK, gormResponse)
}

func gormInsertData(c *gin.Context) {
	var p Product
	err := c.Bind(&p)
	if err != nil {
		gormResponse.Code = http.StatusBadRequest
		gormResponse.Message = "参数错误"
		gormResponse.Data = err
		c.JSON(http.StatusOK, gormResponse)
		return
	}
	tx := gormDB.Create(&p)
	if tx.RowsAffected > 0 {
		gormResponse.Code = http.StatusOK
		gormResponse.Message = "成功"
		gormResponse.Data = err
		c.JSON(http.StatusOK, gormResponse)
	}
}
