## JWT-go:A go implementation of JSON Web Tokens

#### 签名方法和key类型：

The HMAC signing method(HS256,HS384,HS512)//hash消息认证码

The RSA signing method(RS256,RS384,RS512)//RSA非对称加密签名

The ECDSA signing method(ES256,ES384,ES512)//椭圆曲线数字签名

文档地址：https://github.com/golang-jwt/jwt