package main

import (
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)

// http://www.metools.info/code/c80.html
// RSA签名实现token
type RsaUser struct {
	Id        string `json:"id"`
	Name      string `json:"name"`
	Telephone string `json:"telephone"`
	Password  string `json:"password"`
}

var (
	resPrivateKey  []byte
	resPublicKey   []byte
	err2_1, err2_2 error
)

type RsaClaims struct {
	UserId string `json:"user_id"`
	jwt.StandardClaims
}

func init() {
	resPrivateKey, err2_1 = os.ReadFile("./src/token/private.pem")
	resPublicKey, err2_2 = os.ReadFile("./src/token/public.pem")
	if err2_1 != nil || err2_2 != nil {
		log.Panicf("%s\n%s", err2_1, err2_2)
	}
}

func main() {
	r := gin.Default()
	r.POST("getToken2", func(c *gin.Context) {
		var u RsaUser
		err := c.Bind(&u)
		if err != nil {
			c.JSON(http.StatusBadRequest, "参数错误")
			return
		}
		token, err := rsaReleaseToken(u)
		if err != nil {
			c.JSON(http.StatusInternalServerError, err)
			return
		}
		c.JSON(http.StatusOK, gin.H{
			"code": http.StatusOK,
			"msg":  "token分发成功",
			"data": token,
		})
	})

	r.POST("/checkToken2", rsaTokenMiddle(), func(c *gin.Context) {

	})
	r.Run(":9090")
}

// token认证中间件（权限控制）
func rsaTokenMiddle() gin.HandlerFunc {
	return func(c *gin.Context) {
		auth := "zkx"
		// 获取对应token
		tokenString := c.GetHeader("Authorization")
		if tokenString == "" || !strings.HasPrefix(tokenString, auth+":") {
			c.JSON(http.StatusUnauthorized, gin.H{"code": http.StatusUnauthorized, "msg": "前缀错误"})
			c.Abort()
			return
		}
		index := strings.Index(tokenString, auth+":") // 找到token对应的位置
		// 真实的token的值
		tokenString = tokenString[index+len(auth)+1:]
		claims, err := rsaParseToken(tokenString)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"code": http.StatusUnauthorized, "msg": "证书无效"})
			c.Abort()
			return
		}
		claimsValue := claims.(jwt.MapClaims)
		if claimsValue["user_id"] == nil {
			c.JSON(http.StatusUnauthorized, gin.H{"code": http.StatusUnauthorized, "msg": "用户不存在"})
			c.Abort()
			return
		}
		var u RsaUser
		c.Bind(&u)
		if claimsValue["user_id"].(string) == u.Id {
			c.JSON(http.StatusUnauthorized, gin.H{"code": http.StatusUnauthorized, "msg": "用户不存在"})
			c.Abort()
			return
		}
		c.Next()
	}
}

// 解析token
func rsaParseToken(tokenString string) (any, error) {

	pem, err := jwt.ParseRSAPrivateKeyFromPEM(resPublicKey)
	if err != nil {
		return nil, err
	}
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (i any, err error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("解析方法错误")
		}
		return pem, err
	})
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, nil
	}
	return nil, err
}

func rsaReleaseToken(u RsaUser) (any, error) {
	return rsaJwtTokenGen(u.Id)
}

// 生成token
func rsaJwtTokenGen(id string) (any, error) {
	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(resPrivateKey)
	if err != nil {
		return nil, err
	}
	expirationTime := time.Now().Add(7 * 24 * time.Hour)
	claims := RsaClaims{
		UserId: id,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(), // 过期时间
			IssuedAt:  time.Now().Unix(),     // 发布时间
			Subject:   "User token",          // 主题
			Issuer:    "zkx",                 // 发布者
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	SignedString, err := token.SignedString(privateKey)
	return SignedString, err
}
