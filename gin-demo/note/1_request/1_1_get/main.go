package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()    // 路由引擎
	r.GET("/get", getMsg) // get方法
	r.Run(":9090")
}

func getMsg(c *gin.Context) {
	name := c.Query("name")
	// c.String(http.StatusOK, "欢迎您：%s", name)
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"msg":  "返回信息",
		"data": "欢迎您" + name,
	})
}
