package common

import (
	"time"

	"gitee.com/touch40/godemo/gindemo/note/4_practice/model"
	"github.com/golang-jwt/jwt/v4"
)

type MyClaims struct {
	UserId uint
	jwt.StandardClaims
}

var jwtKey = []byte("a_secret_key") // 证书签名秘钥（该秘钥非常重要，如果Client端有该秘钥，就可以分发证书了）

// 解析token
func ParseToken(tokenString string) (*jwt.Token, *MyClaims, error) {
	claims := MyClaims{}
	token, err := jwt.ParseWithClaims(tokenString, &claims, func(token *jwt.Token) (i interface{}, err error) {
		return jwtKey, nil
	})
	return token, &claims, err
}

// 分发token
func ReleaseToken(u model.User) (string, error) {
	expirationTime := time.Now().Add(7 * 24 * time.Hour)
	claims := MyClaims{
		UserId: u.ID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(), // 过期时间
			IssuedAt:  time.Now().Unix(),     // 发布时间
			Subject:   "User token",          // 主题
			Issuer:    "zkx",                 // 发布者
		}}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return "", err
	}
	return tokenString, err
}
