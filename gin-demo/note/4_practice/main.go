package main

import (
	"fmt"
	"os"

	"gitee.com/touch40/godemo/gindemo/note/4_practice/common"
	"gitee.com/touch40/godemo/gindemo/note/4_practice/route"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func main() {
	InitConfig()
	common.InitDB()
	r := gin.Default()
	route.CollectRoute(&r)
	port := viper.GetString("server.port")
	r.Run(":" + port)
}

func InitConfig() {
	workDir, _ := os.Getwd()                                          // 获取目录对应的路径
	viper.SetConfigName("application")                                // 配置文件名
	viper.SetConfigType("yml")                                        // 配置文件类型
	viper.AddConfigPath(workDir + "/config")                          //
	viper.AddConfigPath(workDir + "/src/gin_application" + "/config") //
	fmt.Println(workDir)                                              // 执行go run对应的路径配置
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}
