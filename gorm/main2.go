package main

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

//创建映射结构体
type User struct {
	//gorm.Model框架自带的结构体
	gorm.Model
	Name string
	Age  int64
}

func main2() {
	//连接数据库
	db, err := gorm.Open(mysql.New(mysql.Config{DSN: "root:123456@tcp(localhost:3306)/gormDB?charset=utf8mb4&parseTime=True&loc=Local"}), &gorm.Config{})
	// db, err := gorm.Open("mysql", "root:123456@(localhost:3306)/db01?charset=utf8mb4&parseTime=True&loc=Local")
	if err != nil {
		fmt.Println("open err:", err)
		return
	}
	//关闭连接
	//新版本不需要再手动关闭
	// defer db.Close()

	//与数据表进行关联
	//db.AutoMigrate(&User{})
	//创建数据
	//user := User{Name: "小明", Age: 15}
	//db.Create(&user)
	//user1 := User{Name: "小红", Age: 16}
	//db.Create(&user1)
	//定义一个结构体变量用于接收数据
	var u User

	//查询数据库的数据第一条记录
	db.First(&u)
	fmt.Println("第一条记录为:", u)
	u.Name = "小美"

	//默认修改所有字段
	db.Save(&u)
	db.First(&u)
	fmt.Println("第一条修改记录为:", u)

	//修改指定字段
	db.Model(&u).Update("name", "小天")
	db.First(&u)
	fmt.Println("第一条修改记录Update为:", u)

	//根据条件修改字段
	db.Model(&u).Where("name = ?", "小天").Update("name", "小智")
	db.First(&u)
	fmt.Println("第一条修改记录Update为:", u)

	//使用map进行多字段修改
	db.Model(&u).Updates(map[string]interface{}{"name": "小田", "age": 19})
	db.First(&u)
	fmt.Println("第一条修改记录Updates为:", u)

	//使用结构体修改字段,字段为默认字段不会生效
	db.Model(&u).Updates(User{Name: "小静", Age: 0})
	db.First(&u)
	fmt.Println("第一条修改记录Updates为:", u)

	//使用map进行多字段修改，只修改指定字段
	db.Model(&u).Select("name").Updates(map[string]interface{}{"name": "小飞", "age": 20})

	//排查不修改的字段
	db.Model(&u).Omit("age").Updates(map[string]interface{}{"name": "小飞", "age": 20})
	db.First(&u)
	fmt.Println("第一条修改记录Updates为:", u)

	//只更新指定字段，不会所有字段都更新，单个字段
	db.Model(&u).UpdateColumn("name", "小井")
	db.First(&u)
	fmt.Println("第一条修改记录UpdateColumn为:", u)

	//只更新指定字段，不会所有字段都更新，多个字段
	db.Model(&u).UpdateColumns(User{Name: "小时", Age: 21})
	db.First(&u)
	fmt.Println("第一条修改记录UpdateColumns为:", u)

	//批量更新
	db.Table("users").Where("id in (?)", []int64{1, 2}).Updates(map[string]interface{}{"name": "小丽", "age": 22})
	db.First(&u)
	fmt.Println("第一条修改记录Updates为:", u)

	//获取更新记录总数
	affected := db.Model(&u).Updates(User{Name: "小军", Age: 23}).RowsAffected
	fmt.Println("修改总条数为:", affected)

	//统一修改数据表里的字段
	db.Model(&User{}).Update("age", gorm.Expr("age+?", 2))
	db.First(&u)
	fmt.Println("第一条修改记录Update为:", u)
}
