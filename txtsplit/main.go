package main

import (
	"fmt"
	"os"
)

func main() {
	defer func() {
		e := recover()
		fmt.Printf("%v", e)
	}()
	file, err := os.Open("a.txt")
	if err != nil {
		panic(err)
	}
	fi, err := file.Stat()
	if err != nil {
		panic(err)
	}
	fmt.Printf("%v", fi.Size())
	defer file.Close()
}
