package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

var initialized uint32
var lock sync.Mutex

type singleton struct{}

var instance *singleton

func GetInstance() *singleton {
	//如果标记为被设置，直接返回，不加锁
	if atomic.LoadUint32(&initialized) == 1 {
		return instance
	}
	// 如果没有，则枷锁申请

	lock.Lock()
	defer lock.Unlock()
	if initialized == 0 {
		instance = new(singleton)
		// 设置标记位
		atomic.StoreUint32(&initialized, 1)
	}
	return instance
}

func (s *singleton) SomeThing() {
	fmt.Println("单例对象的某方法")
}

func main() {
	s := GetInstance()
	s.SomeThing()
}
