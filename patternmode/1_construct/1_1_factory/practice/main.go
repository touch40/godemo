package main

import "fmt"

type VideoCard interface {
	Display()
}

type Storage interface {
	Storage()
}

type CPU interface {
	Calculate()
}

type AbstructFactory interface {
	CreateVideoCard() VideoCard
	CreateStorage() Storage
	CreateCpu() CPU
}

type InterVideoCard struct{}

func (interVC *InterVideoCard) Display() {
	fmt.Println("inter video card")
}

type InterStorage struct{}

func (interS *InterStorage) Storage() {
	fmt.Println("inter storage")
}

type InterCPU struct{}

func (interC *InterCPU) Calculate() {
	fmt.Println("inter CPU")
}

type InterFactory struct{}

func (InterFac *InterFactory) CreateVideoCard() VideoCard {
	var ic VideoCard = new(InterVideoCard)
	return ic
}

func (InterFac *InterFactory) CreateStorage() Storage {
	var ic Storage = new(InterStorage)
	return ic
}

func (InterFac *InterFactory) CreateCpu() CPU {
	var ic CPU = new(InterCPU)
	return ic
}

// type ComputerMainBoard interface {
// }

func main() {
	var fac AbstructFactory = new(InterFactory)
	vc := fac.CreateVideoCard()
	vc.Display()
	s := fac.CreateStorage()
	s.Storage()
	c := fac.CreateCpu()
	c.Calculate()
}
