package main

import "fmt"

// ======= 抽象层 =========

//水果类(抽象接口)
type Fruit interface {
	Show() // 接口某方法
}

// ======= 基础类模块 =========

type Apple struct {
}

func (apple *Apple) Show() {
	fmt.Println("我是苹果")
}

type Banana struct {
}

func (banana *Banana) Show() {
	fmt.Println("我是香蕉")
}

type Pear struct {
}

func (pear *Pear) Show() {
	fmt.Println("我是梨")
}

// ========= 工厂模块  =========
//一个工厂， 有一个生产水果的机器，返回一个抽象水果的指针
// type Factory struct{}

// func (fac *Factory) CreateFruit(kind string) Fruit {
// 	var fruit Fruit

// 	if kind == "apple" {
// 		fruit = new(Apple)
// 	} else if kind == "banana" {
// 		fruit = new(Banana)
// 	} else if kind == "pear" {
// 		fruit = new(Pear)
// 	}

// 	return fruit
// }

type AbstractFactory interface {
	CreateFruit() Fruit
}

type AppleFactory struct {
}

func (fac *AppleFactory) CreateFruit() Fruit {
	var fruit Fruit
	fruit = new(Apple)
	return fruit
}

//具体的香蕉工厂
type BananaFactory struct {
	AbstractFactory
}

func (fac *BananaFactory) CreateFruit() Fruit {
	var fruit Fruit

	//生产一个具体的香蕉
	fruit = new(Banana)

	return fruit
}

//具体的梨工厂
type PearFactory struct {
	AbstractFactory
}

func (fac *PearFactory) CreateFruit() Fruit {
	//生产一个具体的梨
	var fruit Fruit = new(Pear)

	return fruit
}
